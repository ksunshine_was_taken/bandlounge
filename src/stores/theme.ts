import { defineStore } from 'pinia'

export const useTheme = defineStore('theme', {
  state: () => {
    let userTheme = localStorage.getItem('theme')
    let osPrefersDarkTheme = window.matchMedia(
      '(prefers-color-scheme: dark)'
    ).matches
    let theme = userTheme === 'dark' || osPrefersDarkTheme ? 'dark' : 'light'

    ToggleDarkModeClassOnDocument(theme)

    return {
      theme: theme,
    }
  },
  actions: {
    async toggle() {
      this.theme = this.theme === 'dark' ? 'light' : 'dark'
      localStorage.setItem('theme', this.theme)

      ToggleDarkModeClassOnDocument(this.theme)
    },
  },
})

function ToggleDarkModeClassOnDocument(theme: string): void {
  if (theme === 'dark') {
    document.documentElement.classList.add('dark')
  } else {
    document.documentElement.classList.remove('dark')
  }
}
