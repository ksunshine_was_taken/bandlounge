import { createApp } from 'vue'
import { createAuth0 } from '@auth0/auth0-vue'
import { createPinia } from 'pinia'
import App from './App.vue'
import './index.css'

// Router
import { Router } from './router'

const app = createApp(App)

console.log(import.meta.env.VITE_AUTH0_REDIRECT_URI)

app.use(
  createAuth0({
    domain: import.meta.env.VITE_AUTH0_DOMAIN,
    client_id: import.meta.env.VITE_AUTH0_CLIENTID,
    redirect_uri: import.meta.env.VITE_AUTH0_REDIRECT_URI,
  })
)
app.use(createPinia())
app.use(Router)

app.mount('#app')
